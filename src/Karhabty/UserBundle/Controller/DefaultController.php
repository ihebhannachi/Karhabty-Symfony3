<?php

namespace Karhabty\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('KarhabtyUserBundle:Default:index.html.twig');
    }
    public function P404Action()
    {
        return $this->render('KarhabtyUserBundle:Default:404.html.twig');
    }
    public function SetImageAction(Request $request)
    {
        if ($request->isMethod('POST'))
        {
            $path=$request->get('upload');
            $user=$this->getUser();
            $user->setPath($path);
            $em=$this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('fos_user_profile_show');
        }
    }
    public function DisplayUserAction()
    {
        $em=$this->getDoctrine()->getManager();
        $users=$em->getRepository('KarhabtyUserBundle:Users')->findBy(array('userRoles'=>'Role_ADMIN'));
        return $this->render('KarhabtyAdminBundle:Users:DisplayUsers.html.twig',array('users'=>$users));
    }
}
