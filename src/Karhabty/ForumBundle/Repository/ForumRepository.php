<?php

/**
 * Created by PhpStorm.
 * User: iheb
 * Date: 26/03/2017
 * Time: 16:32
 */
namespace Karhabty\ForumBundle\Repository;
use Doctrine\ORM\EntityRepository;
class ForumRepository extends EntityRepository
{
    public function findPost($topic)
    {
        $query=$this->getEntityManager()->createQuery("SELECT p FROM KarhabtyForumBundle:Post p WHERE p.topic=:topic AND p.response is null")
        ->setParameter('topic',$topic);
        return $query->getResult();

    }
    public function findReply($topic)
    {
        $query=$this->getEntityManager()->createQuery("SELECT p FROM KarhabtyForumBundle:Post p WHERE p.topic=:topic AND p.response is NOT null")
            ->setParameter('topic',$topic);
        return $query->getResult();

    }
    public function findPostReply($post)
    {
        $query=$this->getEntityManager()->createQuery("SELECT p FROM KarhabtyForumBundle:Post p WHERE  p.response=:post ")
            ->setParameter('post',$post);
        return $query->getResult();

    }
    public function CountPost($topic)
    {
        $query=$this->getEntityManager()->createQuery("SELECT COUNT(p) AS val FROM KarhabtyForumBundle:Post p WHERE p.topic=:topic AND p.response is null")
            ->setParameter('topic',$topic);
        return $query->getResult();

    }
    public function PopularPost()
    {
        $query=$this->getEntityManager()->createQuery("SELECT p FROM KarhabtyForumBundle:Post p ORDER BY  p.likes DESC ");
        $query->setMaxResults(4);
        return $query->getResult();
    }
    public function PopularTopics()
    {
        $query=$this->getEntityManager()->createQuery("SELECT t FROM KarhabtyForumBundle:Topics t ORDER BY  t.views DESC ");
        $query->setMaxResults(4);
        return $query->getResult();
    }
    public function Search($title)
    {
        $query=$this->getEntityManager()->createQuery("SELECT t FROM KarhabtyForumBundle:Topics t WHERE t.title LIKE :title")->setParameter('title',$title);
        return $query->getResult();
    }

}