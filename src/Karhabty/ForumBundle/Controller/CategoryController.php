<?php

namespace Karhabty\ForumBundle\Controller;

use Karhabty\ForumBundle\Entity\Category;
use Karhabty\ForumBundle\Form\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }
    public function AddCategoryAction(Request $request)
    {
        $category=new Category();
        $form=$this->createForm(CategoryType::class,$category);
        if ($form->handleRequest($request)->isValid())
        {
            $em=$this->getDoctrine()->getManager();
            $category->setSlug($category->getName());
            $em->persist($category);
            $em->flush();
            return $this->redirectToRoute('karhabty_admin_Display_Category');
        }
        return $this->render('@KarhabtyAdmin/Forum/AddCategory.html.twig',array('form'=>$form->createView()));
    }
    public function DisplayCategoryAction()
    {
        $em=$this->getDoctrine()->getManager();
        $category=$em->getRepository('KarhabtyForumBundle:Category')->findAll();
        return $this->render('@KarhabtyAdmin/Forum/DisplayCategory.html.twig',array('category'=>$category));
    }
    public function EditCategoryAction($id,Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $category=$em->getRepository('KarhabtyForumBundle:Category')->find($id);
        $form=$this->createForm(CategoryType::class,$category);
        if ($form->handleRequest($request)->isValid())
        {
            $em=$this->getDoctrine()->getManager();
            $category->setSlug($category->getName());
            $em->persist($category);
            $em->flush();
            return $this->redirectToRoute('karhabty_admin_Display_Category');
        }
        return $this->render('@KarhabtyAdmin/Forum/EditCategory.html.twig',array('form'=>$form->createView()));

    }
    public function DeleteCategoryAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $category=$em->getRepository('KarhabtyForumBundle:Category')->find($id);
        $em->remove($category);
        $em->flush();
        return $this->redirectToRoute('karhabty_admin_Display_Category');
    }
    public function DisplayCategoriesAction()
    {
        $em=$this->getDoctrine()->getManager();
        $category=$em->getRepository('KarhabtyForumBundle:Category')->findAll();
        $Ptopic=$em->getRepository('KarhabtyForumBundle:Topics')->PopularTopics();
        return $this->render('@KarhabtyForum/Category/DisplayCategories.html.twig',array('categories'=>$category,'PopularTopic'=>$Ptopic));
    }

}
