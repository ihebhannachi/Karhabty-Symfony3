<?php

namespace Karhabty\ForumBundle\Controller;

use Karhabty\ForumBundle\Entity\Post;
use Karhabty\ForumBundle\Entity\Topics;
use Karhabty\ForumBundle\Form\PostType;
use Karhabty\ForumBundle\Form\TopicsEditType;
use Karhabty\ForumBundle\Form\TopicsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class TopicsController extends Controller
{
    public function DisplayUserTopicsAction()
    {
        $em=$this->getDoctrine()->getManager();
        $topics=$em->getRepository('KarhabtyForumBundle:Topics')->findBy(array('owner'=>$this->getUser()));
        return $this->render('@KarhabtyUser/Forum/MyTopics.html.twig',array('topics'=>$topics));
    }
    public function CreateTopicAction(Request $request)
    {
        $topic= new Topics();
        $form=$this->createForm(TopicsType::class,$topic);
        if ($form->handleRequest($request)->isValid())
        {
            $topic->setSlug($topic->getTitle());
            $topic->setOwner($this->getUser());
            $em=$this->getDoctrine()->getManager();
            $em->persist($topic);
            $em->flush();
            return $this->redirectToRoute('karhabty_user_Topic');
        }
        return $this->render('@KarhabtyForum/Topic/CreateTopic.html.twig',array('form'=>$form->createView()));
    }
    public function DisplayTopicByAllCategoryAction()
    {
        $em=$this->getDoctrine()->getManager();
        $topics=$em->getRepository('KarhabtyForumBundle:Topics')->findAll();
        $category=$em->getRepository('KarhabtyForumBundle:Category')->findAll();
        return $this->render('@KarhabtyForum/Default/index.html.twig',array('topics'=>$topics,'categories'=>$category));
    }
    public function EditTopicAction($id,Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $topic=$em->getRepository('KarhabtyForumBundle:Topics')->find($id);
        $form=$this->createForm(TopicsEditType::class,$topic);
        if ($form->handleRequest($request)->isValid())
        {
            $topic->setSlug($topic->getTitle());
            $topic->setOwner($this->getUser());
            $em->persist($topic);
            $em->flush();
            $this->NewTopicEmailAction($topic);
            return $this->redirectToRoute('karhabty_user_Topic');
        }
        return $this->render('@KarhabtyForum/Topic/EditTopic.html.twig',array('form'=>$form->createView()));
    }
    public function DeleteTopicAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $topic=$em->getRepository('KarhabtyForumBundle:Topics')->find($id);
        $em->remove($topic);
        $em->flush();
        return $this->redirectToRoute('karhabty_user_Topic');
    }
    public function TopicDetailsAction($slugc,$slugt,Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $topic=$em->getRepository('KarhabtyForumBundle:Topics')->findOneBy(array('slug'=>$slugt));
        $category=$em->getRepository('KarhabtyForumBundle:Category')->findBy(array('slug'=>$slugc));
        $post=new Post();
        $postform=$this->createForm(PostType::class,$post);
        if (empty($topic)|| empty($category))
        {
            return $this->redirectToRoute('karhabty_user_404');
        }else{
            $topic->setViews($topic->getViews()+1);
            $em->persist($topic);
            $em->flush();
            $posts=$em->getRepository('KarhabtyForumBundle:Post')->findPost($topic);
            $reply=$em->getRepository('KarhabtyForumBundle:Post')->findReply($topic);
            $count=$em->getRepository('KarhabtyForumBundle:Post')->CountPost($topic);
            //$Ppost=$em->getRepository('KarhabtyForumBundle:Post')->PopularPost();
            $Ptopic=$em->getRepository('KarhabtyForumBundle:Topics')->PopularTopics();
            if($postform->handleRequest($request)->isValid())
            {
                $topic->setLastpost(new \DateTime());
                $post->setTopic($topic);
                $post->setPoster($this->getUser());
                $em->persist($post);
                $em->flush();
                $em->persist($topic);
                $em->flush();
                $redirect=$this->generateUrl('karhabty_forum_Topic_Details',array('slugc'=>$slugc,'slugt'=>$slugt));
                return $this->redirect($redirect);
            }
            return $this->render('@KarhabtyForum/Topic/TopicDetails.html.twig',array('topic'=>$topic,'postform'=>$postform->createView(),
                'posts'=>$posts,'reply'=>$reply,'count'=>$count,'PopularTopic'=>$Ptopic));
        }


    }
    public function DisplayTopicsByCategoryAction($slugc,Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $categories=$em->getRepository('KarhabtyForumBundle:Category')->findAll();
        $category=$em->getRepository('KarhabtyForumBundle:Category')->findOneBy(array('slug'=>$slugc));
        $topic=$em->getRepository('KarhabtyForumBundle:Topics')->findBy(array('category'=>$category));
        $populartopic=$em->getRepository('KarhabtyForumBundle:Topics')->findBy(array('category'=>$category),array('views'=>'DESC'),3);
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate($topic, $request->query->get('page', 1)/*page number*/, 3/*limit per page*/);
        if (empty($category))
        {
            return $this->redirectToRoute('karhabty_user_404');
        }else{
            return $this->render('@KarhabtyForum/Topic/TopicsByCategory.html.twig',array('topics'=>$pagination,'categories'=>$categories,
                'category'=>$category,'PopularTopic'=>$populartopic));
        }
    }
    public function LikeTopicAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $topic=$em->getRepository('KarhabtyForumBundle:Topics')->findOneBy(array('id'=>$id));
        $topic->setLikes($topic->getLikes()+1);
        $em->persist($topic);
        $em->flush();
        $reponse= new JsonResponse();
        return $reponse->setData(array('resultat'=>$topic->getLikes()));

    }
    public function DislikeTopicAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $topic=$em->getRepository('KarhabtyForumBundle:Topics')->findOneBy(array('id'=>$id));
        $topic->setLikes($topic->getLikes()-1);
        $em->persist($topic);
        $em->flush();
        $reponse= new JsonResponse();
        return $reponse->setData(array('resultat'=>$topic->getLikes()));

    }
    public function NewTopicEmailAction($topic)
    {

        $slugc=$topic->getCategory()->getSlug();
        $slugt=$topic->getSlug();
        $name=$topic->getOwner()->getUsername();
        $email=$topic->getOwner()->getEmail();
        $url=$this->generateUrl('karhabty_forum_Topic_Details',array('slugc'=>$slugc,'slugt'=>$slugt));
        $message = \Swift_Message::newInstance()
            ->setSubject('You Have Created a New Topic')
            ->setFrom(array('chikitos.tunisie@gmail.com' => "Karhabty"))
            ->setTo($email)
            ->setBody(
                $this->renderView('KarhabtyForumBundle:Emails:TopicCreated.html.twig', array('name' => $name,'url'=>$url)), 'text/html');
        $this->get('mailer')->send($message);
    }
    public function SearchTopicsAction(Request $request)
    {
        if ($request->isMethod('POST'))
        {
            $title=$request->get('search');
            $title="%".$title."%";
            $em=$this->getDoctrine()->getManager();
            $topic=$em->getRepository('KarhabtyForumBundle:Topics')->Search($title);
            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate($topic, $request->query->get('page', 1)/*page number*/, 4/*limit per page*/);
            return $this->render('@KarhabtyForum/Topic/Search.html.twig',array('topics'=>$pagination));
       }
        return $this->redirectToRoute('karhabty_user_homepage');

    }
    public function DisplayTopicsAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $topics=$em->getRepository('KarhabtyForumBundle:Topics')->findAll();
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate($topics, $request->query->get('page', 1)/*page number*/, 10/*limit per page*/);
        return $this->render('@KarhabtyAdmin/Forum/DisplayTopics.html.twig',array('topics'=>$pagination));
    }
}
