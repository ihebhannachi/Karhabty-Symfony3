<?php

namespace Karhabty\ForumBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReportedType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('reason', ChoiceType::class, array(
            'choices'  => array(
                'Not useful' => 'Not useful',
                'Over-posting' => 'Over-posting',
                'Violation of someone\'s privacy' => 'Violation of someone\'s privacy',
            ),
        ))->add('Report',SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'karhabty_forum_bundle_reported_type';
    }
}
