<?php

namespace Karhabty\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('KarhabtyAdminBundle:Default:index.html.twig');
    }
    public function BannedUserAction($id,Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $user=$em->getRepository('KarhabtyUserBundle:Users')->find($id);
        if (!$user->isBanned())
        {
            $user->setBanned(true);
            $em->persist($user);
            $em->flush();
            $request->getSession()->getFlashBag()->add('success', 'Utilisateurs banni');
            return $this->redirectToRoute('karhabty_admin_Reported_Posts');
        }
        $request->getSession()->getFlashBag()->add('success', 'Utilisateurs deja banni');
        return $this->redirectToRoute('karhabty_admin_Reported_Posts');
    }
    public function DisableUserAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $user=$em->getRepository('KarhabtyUserBundle:Users')->find($id);
        $user->setEnabled(false);
        $em->persist($user);
        $em->flush();
       return $this->redirectToRoute('karhabty_admin_DisplayUsers');
    }
    public function EnableUserAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $user=$em->getRepository('KarhabtyUserBundle:Users')->find($id);
        $user->setEnabled(true);
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute('karhabty_admin_DisplayUsers');
    }
    public function BannedUserAAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $user=$em->getRepository('KarhabtyUserBundle:Users')->find($id);
        $user->setBanned(true);
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute('karhabty_admin_DisplayUsers');
    }
    public function UnBannedUserAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $user=$em->getRepository('KarhabtyUserBundle:Users')->find($id);
        $user->setBanned(false);
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute('karhabty_admin_DisplayUsers');
    }
}
